var myVar;

function bismillah() {
    myVar = setTimeout(showPage, 500);
}

function showPage() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("myDiv").style.display = "block";
}

function gantiWarna() {
    document.getElementsByTagName("body")[0].style.background = "-webkit-linear-gradient(right, #c471ed, #f64f59)";
}

var toggle = document.getElementById('container');
var toggleContainer = document.getElementById('toggle-container');
var toggleNumber;

toggle.addEventListener('click', function () {
    toggleNumber = !toggleNumber;
    if (toggleNumber) {
        toggleContainer.style.clipPath = 'inset(0 0 0 50%)';
        toggleContainer.style.backgroundColor = '#D74046';
        //Mulai Jquery
        $("#register-form").delay(300).fadeIn(300);
        $("#login-form").fadeOut(300);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
        //e.preventDefault();

    } else {
        toggleContainer.style.clipPath = 'inset(0 50% 0 0)';
        toggleContainer.style.backgroundColor = 'dodgerblue';
        //Mulai Jquery
        $("#login-form").delay(300).fadeIn(300);
        $("#register-form").fadeOut(300);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        //e.preventDefault();
    }
    //console.log(toggleNumber)
});

var $inputItem = $(".js-inputWrapper");
$inputItem.length && $inputItem.each(function() {
    //console.log(toggleNumber);
    var $this = $(this),
        $input = $this.find(".formRow--input"),
        //placeholderTxt = $input.attr("placeholder"),
        placeholderTxt = $input.attr("placeholder"),
        $placeholder;

    $input.after('<span class="placeholder">' + placeholderTxt + "</span>"),
        $input.attr("placeholder", ""),
        $placeholder = $this.find(".placeholder"),

        $input.val().length ? $this.addClass("active") : $this.removeClass("active"),

        $input.on("focusout", function() {
            $input.val().length ? $this.addClass("active") : $this.removeClass("active");
        }).on("focus", function() {
            $this.addClass("active");
        });
});